<?php
/**
 * @file
 * Crazier add-ons for profiler builder
 */

/**
 * Implements hook_menu().
 */
function drush_recipes_profiler_builder_menu() {
  $items = array();
  $items['admin/config/development/profiler_builder/drush_recipes'] = array(
    'title' => 'Drush recipes',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('_drush_recipes_profiler_builder_settings'),
    'description' => 'Drush recipe recording console.',
    'access arguments' => array('administer profiler builder'),
    'file' => 'drush_recipes_profiler_builder.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 10,
  );
  return $items;
}

/**
 * Implements hook_profiler_builder_build_files_alter().
 * @param  [array] $files [array keys are filename, value is contents]
 */
function drush_recipes_profiler_builder_profiler_builder_build_files_alter(&$files) {
  $files['auto_recipe.drecipe'] = drush_recipes_profiler_builder_build_file();
}

/**
 * Helper to build the file to deliver.
 * @return [string] [file contents for the recipe file]
 */
function drush_recipes_profiler_builder_build_file($format = 'yaml') {
  // first part of the file
  $output = array(
    'name' => 'Security update status',
    'drush_recipes_api' => '1.0',
    'dependencies' => array('update'),
  );
  // load the queries from the temp directory
  $query_files = _profiler_builder_extras_load_query_files(FALSE);
  $calls = array();
  // loop through the query files
  foreach ($query_files as $queries) {
    foreach ($queries as $query) {
      $call = array();
      // @todo test for the types of query that we can map to a drush function
      // add update vars
      if (strpos($query['query'], 'variable SET value')) {
        $call[] = 'ev';
        $var = unserialize($query['args'][':db_update_placeholder_0']);
        $call[] = 'variable_set(\'' . $query['args'][':db_condition_placeholder_0'] . '\', ' . var_export($var, TRUE) . ')';
      }
      // add insert vars
      elseif(strpos($query['query'], 'INTO variable (name, value)')) {
        $call[] = 'ev';
        $var = unserialize($query['args'][':db_insert_placeholder_1']);
        $call[] = 'variable_set(\'' . $query['args'][':db_insert_placeholder_0'] . '\', ' . var_export($var, TRUE) . ')';
      }
      if (!empty($call)) {
        $calls[] = $call;
      }
    }
  }
  // make sure this actually has a recipe to work
  if (!empty($calls)) {
    $output['recipe'] = $calls;
  }
  // add the metadata
  $output['metadata'] = array(
    'description' => 'Report on the update status of known security related issues.',
    'version' => '1.0',
    'author' => 'btopro'
  );
  // encode for export
  return _drush_recipes_encode($output, $format);
}
